<footer class="footer" id="footer">
		<div class="footer-middle">
			<div class="container">
				<div class="row">
					<!--<div class="col-xs-6 pull-right">
						<div class="social-buttons">
							<ul class="social-buttons__list horizontal">
								<li class="social-buttons__item"><a class="sb-vk" href="#"></a></li>
								<li class="social-buttons__item"><a class="sb-tw" href="#"></a></li>
								<li class="social-buttons__item"><a class="sb-yo" href="#"></a></li>
								<li class="social-buttons__item"><a class="sb-ml" href="#"></a></li>
								<li class="social-buttons__item"><a class="sb-fb" href="#"></a></li>
								<li class="social-buttons__item"><a class="sb-ot" href="#"></a></li>
							</ul>
						</div>
					</div>-->
					<div class="col-xs-6">
						<div class="footer-middle__logo"><a href="<?php echo get_site_url(); ?>" style="text-decoration:none; color:#ffffff;"><h3  class="logocustomstylesmall">Stack<span style="color:#ADD8E6;">Savings</span></h3><small style="font-size:7px; margin-top:-10px; margin-left:44px; position:absolute;">Affordable custom programming</small></a></div>
						<!--<div class="footer-middle__design">
							Design <a href="#">Stacksaving</a>
							
						</div>-->
					</div>
				</div>
			</div>
		</div><!-- end .footer-middle -->
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<nav class="footer-bottom__nav">
                     <?php wp_nav_menu(array('theme_location' => 'in_header','fallback_cb' => 'customFotterMenu')); ?>
						<!--<ul class="footer-bottom__nav__list horizontal">
							<li class="footer-bottom__nav__item"><a href="#">My Account</a></li>
							<li class="footer-bottom__nav__item"><a href="#">Register</a></li>
							<li class="footer-bottom__nav__item"><a href="#">Login</a></li>
							<li class="footer-bottom__nav__item"><a href="#">Wish List</a><span class="count-number">7</span></li>
							<li class="footer-bottom__nav__item"><a href="#">Shopping Cart</a></li>
							<li class="footer-bottom__nav__item"><a href="#">Checkout</a></li>
						</ul>-->
					</nav>
				</div>
			</div>
		</div><!-- end .footer-bottom -->
	</footer><!-- end .footer -->
<div class="sepia-filter"></div>
<!-- end .sepia-filter -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery-1.11.2.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.formstyler.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.flexslider-min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/masonry.pkgd.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.wow.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.nouislider.all.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.barrating.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.validate.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/mans-main.js"></script>
    