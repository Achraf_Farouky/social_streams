<?php
defined('ABSPATH') or die("please don't runs scripts");
/*
* @file           archive.php
* @package        Social Magazine
* @author         ThemesMatic
* @copyright      2015 ThemesMatic
*/
get_header(); 
?>
 <div class="main" id="main">
		<div class="breadcrumbs">
			<div class="container">
				<div class="row">
                <?php
				$categories = get_the_category($post->ID);
				$catPost = get_posts(array('category'=>$categories[0]->cat_ID)); 
/*global $post;
//$categories = get_the_category($post->ID);
echo "<pre>";
print_r($catPost);
echo "</pre>";*/

				 $currentCat = get_query_var('cat');
				  $catData = get_category( $currentCat );	
				
?>
					<ul class="breadcrumbs__lsit horizontal">
						<li>
							<a href="index.php">Index</a>
						</li>
                        <?php if($catData->parent!=0) { 
						$catParentName = get_category($catData->parent);
						?>
						<li>
							<a href="<?php echo get_category_link( $catParentName->cat_ID ); ?>"><?php echo $catParentName->name;?></a>
						</li>
                    		<li>
							<a href="<?php echo get_category_link( $catData->cat_ID ); ?>"><?php echo $catData->name;?></a>
						</li>
                        <?php } else { ?>
							<li>
							<a href="<?php echo get_category_link( $catData->cat_ID ); ?>"><?php echo $catData->name;?></a>
						</li>
							
						<?php 	} ?>
 					</ul>
				</div>
			</div>
		</div><!-- end .breadcrumbs --> 
        <div class="our-blog">
			<div class="page__head">
				<div class="container">
					<div class="row page__head__row">
						<div class="col-lg-12">
							<div class="page__head__container">
								<h1>
                                <?php 
								if($catData->parent!=0){
									$catParentName = get_category($catData->parent);
										if($catParentName->cat_ID==CONSTANT_BLOG)
										{	echo "Articles"; }
										elseif($catParentName->cat_ID==CONSTANT_PRODUCT){
										echo "Products"; }
									} else
									{
										if($catData->cat_ID==CONSTANT_BLOG)
										{	echo "Articles"; }
										elseif($catData->cat_ID==CONSTANT_PRODUCT){
										echo "Products"; }
									
									}?>
									</h1>
								<nav class="dark__nav" id="dark-nav">
									<!--<a class="dark__nav__category" href="#">Categories</a>-->
                                     <ul class="dark__nav__list horizontal">
                                   		<?php get_sidebar(); ?>
                                  </ul>
								</nav>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="container" >
				<div class="row our-blog__row">
                
                
                <?php
				$currentCat = get_query_var('cat');	
				if($currentCat==CONSTANT_BLOG)
				{
					$args = array(	'child_of'=>CONSTANT_BLOG);
					$allSubCategories = get_categories($args);
					$argsCat = array('category' => $allSubCategories[0]->term_id );
				 	$catPosts = get_posts( $argsCat);
				}
				else
				{
					$argsCat = array('category' =>$currentCat);
				 	$catPosts = get_posts( $argsCat);
				}
					/*echo "<pre>";
					print_r($catPosts);
					echo "</pre>";*/
					?>
					
					<!--<div class="col-md-6  col-sm-12 wow fadeInUp animated" data-wow-delay="0.6s" data-wow-offset="50" data-wow-duration="1s">
						<div class="main-news__box photo-bg">
							<div class="main-news__box__container">
								<h3><a href="#">Fashion Black Stainless Steel Luxury</a></h3>
								<p>Investment, implementation non-partisan Gandhi life-saving enable collaborative cities women's rights change ...</p>
							</div>
							<div class="main-news__box__date">23.02.2014</div>
							<img src="http://placehold.it/540x531" alt=""/>
						</div>
					</div>-->
                    <div style="width:100%;float:left;">
                    <?php foreach($catPosts as $singlePost) :
					
					//$customPostId = $singlePost->ID; ?>
					<div class="col-md-3 col-sm-6 col-xs-6 wow fadeInUp animated" data-wow-delay="0.2s" data-wow-offset="50" data-wow-duration="1s">
						<div class="main-news__box">
							<div class="main-news__box__photo">
								<?php 
						 	if ( has_post_thumbnail($singlePost->ID)){ ?>
                           	<a href="<?=$singlePost->guid;?>"><?php echo  get_the_post_thumbnail($singlePost->ID,'thumbnail');?> </a>
                            <div class="main-news__box__date"><?=$singlePost->post_date;?></div>
								<?php }?>
								
							</div>
							<h3><?=$singlePost->post_title;?></a></h3>
                            <p><?php echo substr(strip_tags($singlePost->post_content,''), 0 , 150); ?>...
                            <a href="<?=$singlePost->guid;?>" class="readmore">Read more</a></p>
						</div>
					</div>
                    <?php endforeach;?>
                    </div>
					<!--<div class="col-md-3 col-sm-6 col-xs-6 wow fadeInUp animated" data-wow-delay="0.4s" data-wow-offset="50" data-wow-duration="1s">
						<div class="main-news__box">
							<div class="main-news__box__photo">
								<a href="#">
									<img src="http://placehold.it/255x356" alt=""/>
								</a>
								<div class="main-news__box__date">23.02.2014</div>
							</div>
							<h3><a href="#">Fashion Black Stainless Steel Luxury</a></h3>
							<p>Investment, implementation non-partisan Gandhi life-saving enable collaborative cities women's rights change ...</p>
						</div>
					</div>
				</div>
				<div class="row our-blog__row">
					<div class="col-md-3 col-sm-6 col-xs-6 wow fadeInUp animated" data-wow-delay="0.2s" data-wow-offset="50" data-wow-duration="1s">
						<div class="main-news__box">
							<div class="main-news__box__photo">
								<a href="#">
									<img src="http://placehold.it/255x356" alt=""/>
								</a>
								<div class="main-news__box__date">23.02.2014</div>
							</div>
							<h3><a href="#">Gas 'N Go Mower</a></h3>
							<p>Investment, implementation non-partisan Gandhi life-saving enable collaborative cities women's rights change ...</p>
						</div>
					</div>
					<div class="col-md-3 col-sm-6 col-xs-6 wow fadeInUp animated" data-wow-delay="0.4s" data-wow-offset="50" data-wow-duration="1s">
						<div class="main-news__box">
							<div class="main-news__box__photo">
								<a href="#">
									<img src="http://placehold.it/255x356" alt=""/>
								</a>
								<div class="main-news__box__date">23.02.2014</div>
							</div>
							<h3><a href="#">Fashion Black Stainless Steel Luxury</a></h3>
							<p>Investment, implementation non-partisan Gandhi life-saving enable collaborative cities women's rights change ...</p>
						</div>
					</div>
					<div class="col-md-6  col-sm-12 wow fadeInUp animated" data-wow-delay="0.6s" data-wow-offset="50" data-wow-duration="1s">
						<div class="main-news__box photo-bg">
							<div class="main-news__box__container">
								<h3><a href="#">Fashion Black Stainless Steel Luxury</a></h3>
								<p>Investment, implementation non-partisan Gandhi life-saving enable collaborative cities women's rights change ...</p>
							</div>
							<div class="main-news__box__date">23.02.2014</div>
							<img src="http://placehold.it/540x531" alt=""/>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="best-sellers__btn">
						<ul class="best-sellers__btn__list horizontal">
							<li class="best-sellers__btn__item"><a class="btn medium gray icon-load" href="#">Load more</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>--><!-- end .our-blog -->
		<!--<nav class="main-nav">
			<div class="container">
				<div class="row">
					<div class="col-xs-3 wow fadeInUp animated" data-wow-delay="0.2s" data-wow-offset="50" data-wow-duration="1s">
						<p>Information</p>
						<ul class="main-nav__list">
							<li><a href="#">About Us</a></li>
							<li><a href="#">Delivery Information</a></li>
							<li><a href="#">Privacy Policy</a></li>
							<li><a href="#">Terms &amp; Conditions</a></li>
						</ul>
					</div>
					<div class="col-xs-3 wow fadeInUp animated" data-wow-delay="0.4s" data-wow-offset="50" data-wow-duration="1s">
						<p>Extras</p>
						<ul class="main-nav__list">
							<li><a href="#">Brands</a></li>
							<li><a href="#">Gift Vouchers</a></li>
							<li><a href="#">Affiliates</a></li>
							<li><a href="#">Specials</a></li>
						</ul>
					</div>
					<div class="col-xs-3 wow fadeInUp animated" data-wow-delay="0.6s" data-wow-offset="50" data-wow-duration="1s">
						<p>My account</p>
						<ul class="main-nav__list">
							<li><a href="#">My Account</a></li>
							<li><a href="#">Order History</a></li>
							<li><a href="#">Wish List</a></li>
							<li><a href="#">Newsletter</a></li>
						</ul>
					</div>
					<div class="col-xs-3 wow fadeInUp animated" data-wow-delay="0.8s" data-wow-offset="50" data-wow-duration="1s">
						<p>Customer service</p>
						<ul class="main-nav__list">
							<li><a href="#">Contact Us</a></li>
							<li><a href="#">Returns</a></li>
							<li><a href="#">Site Map</a></li>
						</ul>
					</div>
				</div>
			</div>
		</nav>--><!-- end .main-nav -->
		
	</div>
        </div>
<?php get_footer(); ?>