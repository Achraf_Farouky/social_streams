<?php
defined('ABSPATH') or die("please don't runs scripts");
/*
* @file           single.php
* @package        Social Magazine
* @author         ThemesMatic
* @copyright      2015 ThemesMatic
*/
get_header();
/************************* to load blog template ************************/
$post = $wp_query->post;
$categories = get_the_category($post->ID);

//if (in_category('Blog') ) {
if($categories[0]->category_parent==CONSTANT_BLOG){	
	include ('single-blog.php');
	return;
}
else 
{
/**************************end **************************************/
get_header(); 
 ?>
  
  
	<div class="main" id="main">
		<div class="breadcrumbs">
			<div class="container">
				<div class="row">
                <?php
					$categories = get_the_category($post->ID);
					$catPost = get_posts(array('category'=>$categories[0]->cat_ID)); 
				?>
					<ul class="breadcrumbs__lsit horizontal">
						<li>
							<a href="index.php">Index</a>
						</li>
                      <?php 
					    	$categories = get_the_category($post->ID);
					  		$catData = get_category($categories[0]->cat_ID);
								if($catData->parent!=0) { 
									$catParentName = get_category($catData->parent);
								?>
                                <li>
							<a href="<?php echo get_category_link( $catParentName->cat_ID ); ?>"><?php echo $catParentName->name;?></a>
						</li>
                    		<li>
							<a href="<?php echo get_category_link( $catData->cat_ID ); ?>"><?php echo $catData->name;?></a>
						</li>
                        <?php } else { ?>
							<li>
							<a href="<?php echo get_category_link( $catData->cat_ID ); ?>"><?php echo $catData->name;?></a>
						</li>
							
						<?php 	} ?>
						
						<li>
							<?php echo get_the_title($post->ID);?>
						</li>
					</ul>
				</div>
			</div>
		</div><!-- end .breadcrumbs -->
		<div class="block-product">
			<div class="container">
				<div class="row block-product__row">
                	<?php 
						 	if ( has_post_thumbnail() ) {
		 						$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' );
								 $url = $thumb['0'];?>
					<div class="col-sm-6">
						<div class="flexslider" id="product-slider">
							<ul class="slides">
								<li><img src="<?=$url;?>" alt=""/></li>
								
							</ul>
						</div>
					</div>
                    <?php } ?>
					<div class="col-sm-6">
						<h1><?php echo get_the_title($post->ID);?></h1>
						<!--<div class="block-product__reviews">
							<ul class="block-product__reviews__list horizontal">
								<li>
									<div class="rating big">
										<ul class="rating__list horizontal">
											<li class="rating__item active"></li>
											<li class="rating__item active"></li>
											<li class="rating__item"></li>
											<li class="rating__item"></li>
											<li class="rating__item"></li>
										</ul>
									</div>
								</li>
								<li><a id="product-reviews" href="#">(10 reviews)</a></li>
								<li><a href="#">Write a review</a></li>
							</ul>
						</div>-->
                        <?php 
						$my_postid = $post->ID;//This is page id or post id
						$content_post = get_post($my_postid);
						$content = $content_post->post_content;
						$content = apply_filters('the_content', $content);
						$content = str_replace(']]>', ']]&gt;', $content);
						echo $content;?>
						<!--<div class="block-product__info">
							<table class="block-product__info__table">
								<tr>
									<td>Brand:</td>
									<td><a href="#">Canon</a></td>
								</tr>
								<tr>
									<td>Product Code:</td>
									<td>Product 3</td>
								</tr>
								<tr>
									<td>Reward Points:</td>
									<td>200</td>
								</tr>
								<tr>
									<td>Availability:</td>
									<td>In Stock</td>
								</tr>
							</table>
						</div>
						<div class="block-product__bottom">
							<div class="block-product__bottom__price">$13 500</div>
							<div class="block-product__bottom__btn"><a class="btn shaded red big icon-cart" href="#">Add to cart</a></div>
						</div>-->
						<!--<div class="social-color">
							<ul class="social-color__list horizontal">
								<li><a class="sc-vk" href="#"></a></li>
								<li><a class="sc-tw" href="#"></a></li>
								<li><a class="sc-b" href="#"></a></li>
								<li><a class="sc-fb" href="#"></a></li>
								<li><a class="sc-gl" href="#"></a></li>
							</ul>
						</div>-->
					</div>
				</div>
			</div>
		</div><!-- end .block-product -->
		<div class="product-taber">
			<div class="product-taber__head">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<ul class="product-taber__head__list horizontal">
								<li class="active">
									<a href="#pt-tab1">
										Description
									</a>
								</li>
								<li>
									<a href="#pt-tab2">
										Reviews
										<span class="count-number">21</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="product-taber__body">
				<div class="pt-tab" id="pt-tab1">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 product-taber__description">
								<h2>Lorem ipsum dolor sit amet</h2>
								<p>
									Monsectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus,Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,
									<br>
									Proin lectus ipsum, gravida et
									<br>
									Mattis vulputate, tristique ut lectus,Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus
								</p>
								<h3>Lorem ipsum dolor</h3>
								<ul class="decor-list">
									<li>Monsectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore;</li>
									<li>Lorem ipsum dolor sit amet, consectetur adipisicing;</li>
									<li>Ut enim ad minim veniam, quis nostrud exercitation ullamco;</li>
									<li>Proin lectus ipsum, gravida;</li>
								</ul>
								<p>
									Monsectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus,Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,
									<br>
									Proin lectus ipsum, gravida et
									<br>
									Mattis vulputate, tristique ut lectus,Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco,Proin lectus ipsum, gravida et mattis vulputate, tristique ut lectus
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="pt-tab" id="pt-tab2">
					<div class="block-comments">
						<div class="container">
							<div class="row">
								<div class="col-sm-8 col-md-9">
									<ul class="block-comments__list">
										<li class="block-comments__row">
											<div class="block-comments__top">
												<span class="name">Mattew An</span>
												<div class="rating">
													<ul class="rating__list horizontal">
														<li class="rating__item active"></li>
														<li class="rating__item active"></li>
														<li class="rating__item"></li>
														<li class="rating__item"></li>
														<li class="rating__item"></li>
													</ul>
												</div>
												<span class="date">21.11.2014</span>
											</div>
											<div class="block-comments__cloud">
												Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
											</div>
										</li>
										<li class="block-comments__row">
											<div class="block-comments__top">
												<span class="name">Mattew An</span>
												<div class="rating">
													<ul class="rating__list horizontal">
														<li class="rating__item active"></li>
														<li class="rating__item"></li>
														<li class="rating__item"></li>
														<li class="rating__item"></li>
														<li class="rating__item"></li>
													</ul>
												</div>
												<span class="date">21.11.2014</span>
											</div>
											<div class="block-comments__cloud">
												Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod 
											</div>
										</li>
										<li class="block-comments__row">
											<div class="block-comments__top">
												<span class="name">Mattew An</span>
												<div class="rating">
													<ul class="rating__list horizontal">
														<li class="rating__item"></li>
														<li class="rating__item"></li>
														<li class="rating__item"></li>
														<li class="rating__item"></li>
														<li class="rating__item"></li>
													</ul>
												</div>
												<span class="date">21.11.2014</span>
											</div>
											<div class="block-comments__cloud">
												Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
											</div>
										</li>
										<li class="block-comments__row">
											<div class="block-comments__top">
												<span class="name">Mattew An</span>
												<div class="rating">
													<ul class="rating__list horizontal">
														<li class="rating__item active"></li>
														<li class="rating__item active"></li>
														<li class="rating__item active"></li>
														<li class="rating__item active"></li>
														<li class="rating__item"></li>
													</ul>
												</div>
												<span class="date">21.11.2014</span>
											</div>
											<div class="block-comments__cloud">
												Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
											</div>
										</li>
										<li class="block-comments__row">
											<div class="block-comments__top">
												<span class="name">Mattew An</span>
												<div class="rating">
													<ul class="rating__list horizontal">
														<li class="rating__item active"></li>
														<li class="rating__item active"></li>
														<li class="rating__item active"></li>
														<li class="rating__item active"></li>
														<li class="rating__item active"></li>
													</ul>
												</div>
												<span class="date">21.11.2014</span>
											</div>
											<div class="block-comments__cloud">
												Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod 
											</div>
										</li>
									</ul>
								</div>
								<div class="col-sm-4 col-md-3">
									<div class="comments-add">
										<form action="#" method="post">
											<div class="comments-add__title">Add review</div>
											<ul class="comments-add__list">
												<li class="comments-add__item">
													<input type="text" class="input-text white" placeholder="Your name"/>
												</li>
												<li class="comments-add__item">
													<input type="text" class="input-text white" placeholder="Your E-Mail"/>
												</li>
												<li class="comments-add__item">
													<div class="rating-plugin">
														<label>Rate:</label>
														<div class="rp-container">
															<div class="input select rating-c">
																<select class="rp-stars" name="rating">
																	<option value=""></option>
																	<option value="1">1</option>
																	<option value="2">2</option>
																	<option value="3" selected="selected">3</option>
																	<option value="4">4</option>
																	<option value="5">5</option>
																</select>
															</div>
														</div>
													</div>
												</li>
												<li class="comments-add__item">
													<textarea cols="2" rows="2" class="textarea-text white" placeholder="Review"></textarea>
												</li>
												<li class="comments-add__item">
													<button type="Submit" class="btn shaded red medium">Submit</button>
												</li>
											</ul>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div><!-- end .product-taber -->
		<div class="best-sellers">
			<div class="best-sellers__top">
				<div class="container">
					<div class="row">
						<h2>Compare products</h2>
					</div>
				</div>
			</div>
			<div class="best-sellers__middle">
				<div id="container-more">
                
                <?php foreach ($catPost as $tempposts) {
					
					?>
				  
                  
					<div class="item">
						<div class="item__container">
							<a href="<?php echo esc_url( get_permalink($tempposts->ID) ); ?>"><img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($tempposts->ID));?> " alt=""/> </a>
							<span class="item__info">
								<span class="item__info__title">Fashion Black Stainless Steel Luxury</span>
								<span class="item__info__bottom">
									<span class="item__info__price">$1 900</span>
									<a class="item__info__cart icon-cart-small" href="#cart">cart</a>
								</span>
								<a class="item__info__link" href="#product"></a>
							</span>
						</div>
					</div>
                     <?php }?>
				</div>
                	<!--<div class="item">
						<div class="item__container">	
							
							<span class="item__info">
								<span class="item__info__title">Fashion Black Stainless Steel Luxury</span>
								<span class="item__info__bottom">
									<span class="item__info__price">$1 900</span>
									<a class="item__info__cart icon-cart-small" href="#cart">cart</a>
								</span>
								<a class="item__info__link" href="#product"></a>
							</span>
						</div>
					</div>
					<div class="item">
						<div class="item__container">
							<img src="http://placehold.it/257x322" alt=""/>
							<span class="item__info">
								<span class="item__info__title">Fashion Black Stainless Steel Luxury</span>
								<span class="item__info__bottom">
									<span class="item__info__price">$1 900</span>
									<a class="item__info__cart icon-cart-small" href="#cart">cart</a>
								</span>
								<a class="item__info__link" href="#product"></a>
							</span>
						</div>
					</div>
					<div class="item">
						<div class="item__container">
							<img src="http://placehold.it/257x322" alt=""/>
							<span class="item__info">
								<span class="item__info__title">Fashion Black Stainless</span>
								<span class="item__info__bottom">
									<span class="item__info__price">$1 900</span>
									<a class="item__info__cart icon-cart-small" href="#cart">cart</a>
								</span>
								<a class="item__info__link" href="#product"></a>
							</span>
						</div>
					</div>
					<div class="item">
						<div class="item__container">
							<img src="http://placehold.it/257x172" alt=""/>
							<span class="item__info">
								<span class="item__info__title">Fashion Black Stainless Steel Luxury</span>
								<span class="item__info__bottom">
									<span class="item__info__price">$1 900</span>
									<a class="item__info__cart icon-cart-small" href="#cart">cart</a>
								</span>
								<a class="item__info__link" href="#product"></a>
							</span>
						</div>
					</div>
					<div class="item">
						<div class="item__container">
							<img src="http://placehold.it/257x322" alt=""/>
							<span class="item__info">
								<span class="item__info__title">Fashion Black Stainless Steel Luxury</span>
								<span class="item__info__bottom">
									<span class="item__info__price">$1 900</span>
									<a class="item__info__cart icon-cart-small" href="#cart">cart</a>
								</span>
								<a class="item__info__link" href="#product"></a>
							</span>
						</div>
					</div>
					<div class="item">
						<div class="item__container">
							<img src="http://placehold.it/257x322" alt=""/>
							<span class="item__info">
								<span class="item__info__title">Fashion Black Stainless Steel Luxury</span>
								<span class="item__info__bottom">
									<span class="item__info__price">$1 900</span>
									<a class="item__info__cart icon-cart-small" href="#cart">cart</a>
								</span>
								<a class="item__info__link" href="#product"></a>
							</span>
						</div>
					</div>	
					<div class="item w2">
						<div class="item__container">
							<img src="http://placehold.it/533x232" alt=""/>
							<span class="item__info">
								<span class="item__info__title">Fashion Black Stainless</span>
								<span class="item__info__bottom">
									<span class="item__info__price">$1 900</span>
									<a class="item__info__cart icon-cart-small" href="#cart">cart</a>
								</span>
								<a class="item__info__link" href="#product"></a>
							</span>
						</div>
					</div>
					<div class="item">
						<div class="item__container">
							<img src="http://placehold.it/257x322" alt=""/>
							<span class="item__info">
								<span class="item__info__title">Fashion Black Stainless Steel Luxury</span>
								<span class="item__info__bottom">
									<span class="item__info__price">$1 900</span>
									<a class="item__info__cart icon-cart-small" href="#cart">cart</a>
								</span>
								<a class="item__info__link" href="#product"></a>
							</span>
						</div>
					</div>
					<div class="item">
						<div class="item__container">
							<img src="http://placehold.it/257x172" alt=""/>
							<span class="item__info">
								<span class="item__info__title">Fashion Black Stainless Steel Luxury</span>
								<span class="item__info__bottom">
									<span class="item__info__price">$1 900</span>
									<a class="item__info__cart icon-cart-small" href="#cart">cart</a>
								</span>
								<a class="item__info__link" href="#product"></a>
							</span>
						</div>
					</div>
					<div class="item">
						<div class="item__container">
							<img src="http://placehold.it/257x429" alt=""/>
							<span class="item__info">
								<span class="item__info__title">Fashion Black Stainless Steel Luxury</span>
								<span class="item__info__bottom">
									<span class="item__info__price">$1 900</span>
									<a class="item__info__cart icon-cart-small" href="#cart">cart</a>
								</span>
								<a class="item__info__link" href="#product"></a>
							</span>
						</div>
					</div>
					<div class="item item__height">
						<div class="item__container">
							<img src="http://placehold.it/257x232" alt=""/>
							<span class="item__info">
								<span class="item__info__title">Fashion Black Stainles</span>
								<span class="item__info__bottom">
									<span class="item__info__price">$1 900</span>
									<a class="item__info__cart icon-cart-small" href="#cart">cart</a>
								</span>
								<a class="item__info__link" href="#product"></a>
							</span>
						</div>
					</div>
					<div class="item">
						<div class="item__container">
							<img src="http://placehold.it/257x232" alt=""/>
						</div>
						<span class="item__discount">
							<a href="#">
								<span class="item__discount__percentage"><span>-10%</span></span>
							</a>
						</span>
						<span class="item__info">
							<span class="item__info__title">Fashion Black Stainless</span>
							<span class="item__info__bottom">
								<span class="item__info__price">$1 900</span>
								<a class="item__info__cart icon-cart-small" href="#cart">cart</a>
							</span>
							<a class="item__info__link" href="#product"></a>
						</span>
					</div>
					<div class="item">
						<div class="item__container">
							<img src="http://placehold.it/257x232" alt=""/>
						</div>
						<span class="item__info">
							<span class="item__info__title">Fashion Black Stainless</span>
							<span class="item__info__bottom">
								<span class="item__info__price">$1 900</span>
								<a class="item__info__cart icon-cart-small" href="#cart">cart</a>
							</span>
							<a class="item__info__link" href="#product"></a>
						</span>
					</div>
					<div class="item">
						<div class="item__container">
							<img src="http://placehold.it/257x171" alt=""/>
						</div>
						<span class="item__info">
							<span class="item__info__title">Fashion Black Stainless</span>
							<span class="item__info__bottom">
								<span class="item__info__price">$1 900</span>
								<a class="item__info__cart icon-cart-small" href="#cart">cart</a>
							</span>
							<a class="item__info__link" href="#product"></a>
						</span>
					</div>
					<div class="item">
						<div class="item__container">
							<img src="http://placehold.it/257x171" alt=""/>
						</div>
						<span class="item__discount">
							<a href="#">
								<span class="item__discount__percentage"><span>-10%</span></span>
							</a>
						</span>
						<span class="item__info">
							<span class="item__info__title">Fashion Black Stainless</span>
							<span class="item__info__bottom">
								<span class="item__info__price">$1 900</span>
								<a class="item__info__cart icon-cart-small" href="#cart">cart</a>
							</span>
							<a class="item__info__link" href="#product"></a>
						</span>
					</div>-->
				</div>
			</div>
			<div class="best-sellers__bottom best-sellers__load">
				<div class="container">
					<div class="row">
						<div class="best-sellers__btn">
							<ul class="best-sellers__btn__list horizontal">
								<li class="best-sellers__btn__item"><a class="btn medium gray icon-load" href="#">Load more</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div><!-- end .best-sellers -->
		<!--<nav class="main-nav">
			<div class="container">
				<div class="row">
					<div class="col-xs-3 wow fadeInUp animated" data-wow-delay="0.2s" data-wow-offset="50" data-wow-duration="1s">
						<p>Information</p>
						<ul class="main-nav__list">
							<li><a href="#">About Us</a></li>
							<li><a href="#">Delivery Information</a></li>
							<li><a href="#">Privacy Policy</a></li>
							<li><a href="#">Terms &amp; Conditions</a></li>
						</ul>
					</div>
					<div class="col-xs-3 wow fadeInUp animated" data-wow-delay="0.4s" data-wow-offset="50" data-wow-duration="1s">
						<p>Extras</p>
						<ul class="main-nav__list">
							<li><a href="#">Brands</a></li>
							<li><a href="#">Gift Vouchers</a></li>
							<li><a href="#">Affiliates</a></li>
							<li><a href="#">Specials</a></li>
						</ul>
					</div>
					<div class="col-xs-3 wow fadeInUp animated" data-wow-delay="0.6s" data-wow-offset="50" data-wow-duration="1s">
						<p>My account</p>
						<ul class="main-nav__list">
							<li><a href="#">My Account</a></li>
							<li><a href="#">Order History</a></li>
							<li><a href="#">Wish List</a></li>
							<li><a href="#">Newsletter</a></li>
						</ul>
					</div>
					<div class="col-xs-3 wow fadeInUp animated" data-wow-delay="0.8s" data-wow-offset="50" data-wow-duration="1s">
						<p>Customer service</p>
						<ul class="main-nav__list">
							<li><a href="#">Contact Us</a></li>
							<li><a href="#">Returns</a></li>
							<li><a href="#">Site Map</a></li>
						</ul>
					</div>
				</div>
			</div>
		</nav>--><!-- end .main-nav -->
		<!--<div class="pay-systems">
			<div class="container">
				<div class="row">
					<div class="pay-systems__buffer">
						<ul>
							<li><a class="visa" href="#"></a></li>
							<li><a class="matercard" href="#"></a></li>
							<li><a class="webmoney" href="#"></a></li>
							<li><a class="alfa" href="#"></a></li>
							<li><a class="paypal" href="#"></a></li>
							<li><a class="unistream" href="#"></a></li>
							<li><a class="ukash" href="#"></a></li>
							<li><a class="easypay" href="#"></a></li>
							<li><a class="skrill" href="#"></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>--><!-- end .pay-systems -->
	</div><!-- end .main -->
	
		<?php //get_sidebar();
		 } ?>

<?php get_footer(); ?>