<?php
defined('ABSPATH') or die("please don't runs scripts");
/*
* @file           single.php
* @package        Social Magazine
* @author         ThemesMatic
* @copyright      2015 ThemesMatic
*/
//get_header(); ?>
              <?php
				$categories = get_the_category($post->ID);
				$post = $wp_query->post;
			/*echo "<pre>";
				print_r($post);
				die;*/
				//$catPost = get_posts(array('category'=>$categories[0]->cat_ID)); 
?>
<body>
    <div class="main" id="main">
		<div class="breadcrumbs">
			<div class="container">
				<div class="row">
			<?php 		
				 $currentCat = $categories[0]->cat_ID;
				  $catData = get_category( $currentCat );	
				
?>
					<ul class="breadcrumbs__lsit horizontal">
						<li>
							<a href="index.php">Index</a>
						</li>
                        <?php if($catData->parent!=0) { 
						$catParentName = get_category($catData->parent);
						?>
						<li>
							<a href="<?php echo get_category_link( $catParentName->cat_ID ); ?>"><?php echo $catParentName->name;?></a>
						</li>
                    		<li>
							<a href="<?php echo get_category_link( $catData->cat_ID ); ?>"><?php echo $catData->name;?></a>
						</li>
                        <?php } else { ?>
							<li>
							<a href="<?php echo get_category_link( $catData->cat_ID ); ?>"><?php echo $catData->name;?></a>
						</li>
							
						<?php 	} ?>
                        <li>
							<?php echo $post->post_name;?></a>
						</li>
 					</ul>
				</div>
			</div>
		</div><!-- end .breadcrumbs -->
		<div class="our-blog inside">
			<div class="page__head">
				<div class="container">
					<div class="row page__head__row">
						<div class="col-lg-12">
							<div class="page__head__container">
								<h1><?php 
								if($catData->parent!=0){
									$catParentName = get_category($catData->parent);
										if($catParentName->cat_ID==CONSTANT_BLOG)
										{	echo "Articles"; }
										elseif($catParentName->cat_ID==CONSTANT_PRODUCT){
										echo "Products"; }
									} else
									{
										if($catData->cat_ID==CONSTANT_BLOG)
										{	echo "Articles"; }
										elseif($catData->cat_ID==CONSTANT_PRODUCT){
										echo "Products"; }
									
									}?></h1>
								<nav class="dark__nav" id="dark-nav">
									<!--<a class="dark__nav__category" href="#">Categories</a>-->
									  <ul class="dark__nav__list horizontal">
                                   		<?php
											$post = $wp_query->post;
											$currentCatDetails = get_the_category($post->ID);
											$args = array(	'child_of'=>4); 
											$allSubCategories = get_categories($args);
											foreach($allSubCategories as $subCat): ?>
                                                <li><a href="<?=get_category_link( $subCat->term_id );?>">
                                                <?=$subCat->name;?></a></li>
											<?php endforeach;?>

 
										
                                        </ul>
								</nav>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="our-blog__promo">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<h2><?=$post->post_title;?></h2>
							<!--<p>Investment, implementation non-partisan Gandhi life-saving enable collaborative cities women's rights change ...</p>-->
							<div class="date"><?php echo $post->post_date;?></div>
							<!--<div class="social__likes">
								<ul class="social__likes__list horizontal">
									<li class="social__likes__item">
										<a href="#">
											<span class="icon hz1"></span>
											<span class="count">1290</span>
										</a>
									</li>
									<li class="social__likes__item">
										<a href="#">
											<span class="icon hz2"></span>
											<span class="count">1290</span>
										</a>
									</li>
									<li class="social__likes__item">
										<a href="#">
											<span class="icon fb"></span>
											<span class="count">1290</span>
										</a>
									</li>
									<li class="social__likes__item">
										<a href="#">
											<span class="icon tw"></span>
											<span class="count">1290</span>
										</a>
									</li>
									<li class="social__likes__item">
										<a href="#">
											<span class="icon vk"></span>
											<span class="count">1290</span>
										</a>
									</li>
									<li class="social__likes__item">
										<a href="#">
											<span class="icon hz3"></span>
											<span class="count">1290</span>
										</a>
									</li>
								</ul>
							</div>-->
						</div>
					</div>
				</div>
			</div>
			<div class="our-blog__text">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<?=$post->post_content;?>
						</div>
					</div>
				</div>
			</div>
			<!--<div class="block-comments">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="block-comments__head gray">
								Comments
								<span class="count-number">21</span>
							</div>
						</div>
						<div class="col-sm-8 col-md-9">
							<ul class="block-comments__list">
								<li class="block-comments__row">
									<div class="block-comments__top">
										<span class="name">Mattew An</span>
										<div class="rating">
											<ul class="rating__list horizontal">
												<li class="rating__item active"></li>
												<li class="rating__item active"></li>
												<li class="rating__item"></li>
												<li class="rating__item"></li>
												<li class="rating__item"></li>
											</ul>
										</div>
										<span class="date">21.11.2014</span>
									</div>
									<div class="block-comments__cloud">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
									</div>
								</li>
								<li class="block-comments__row">
									<div class="block-comments__top">
										<span class="name">Mattew An</span>
										<div class="rating">
											<ul class="rating__list horizontal">
												<li class="rating__item active"></li>
												<li class="rating__item"></li>
												<li class="rating__item"></li>
												<li class="rating__item"></li>
												<li class="rating__item"></li>
											</ul>
										</div>
										<span class="date">21.11.2014</span>
									</div>
									<div class="block-comments__cloud">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod 
									</div>
								</li>
								<li class="block-comments__row">
									<div class="block-comments__top">
										<span class="name">Mattew An</span>
										<div class="rating">
											<ul class="rating__list horizontal">
												<li class="rating__item"></li>
												<li class="rating__item"></li>
												<li class="rating__item"></li>
												<li class="rating__item"></li>
												<li class="rating__item"></li>
											</ul>
										</div>
										<span class="date">21.11.2014</span>
									</div>
									<div class="block-comments__cloud">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
									</div>
								</li>
								<li class="block-comments__row">
									<div class="block-comments__top">
										<span class="name">Mattew An</span>
										<div class="rating">
											<ul class="rating__list horizontal">
												<li class="rating__item active"></li>
												<li class="rating__item active"></li>
												<li class="rating__item active"></li>
												<li class="rating__item active"></li>
												<li class="rating__item"></li>
											</ul>
										</div>
										<span class="date">21.11.2014</span>
									</div>
									<div class="block-comments__cloud">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
									</div>
								</li>
								<li class="block-comments__row">
									<div class="block-comments__top">
										<span class="name">Mattew An</span>
										<div class="rating">
											<ul class="rating__list horizontal">
												<li class="rating__item active"></li>
												<li class="rating__item active"></li>
												<li class="rating__item active"></li>
												<li class="rating__item active"></li>
												<li class="rating__item active"></li>
											</ul>
										</div>
										<span class="date">21.11.2014</span>
									</div>
									<div class="block-comments__cloud">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod 
									</div>
								</li>
							</ul>
						</div>
						<div class="col-sm-4 col-md-3">
							<div class="comments-add">
								<form action="#" method="post" id="form">
									<div class="comments-add__title">Add comment</div>
									<ul class="comments-add__list">
										<li class="comments-add__item">
											<input type="text" name="name" class="input-text white" placeholder="Your name"/>
										</li>
										<li class="comments-add__item">
											<input type="text" name="email" class="input-text white" placeholder="Your E-Mail"/>
										</li>
										<li class="comments-add__item">
											<div class="rating-plugin">
												<label>Rate:</label>
												<div class="rp-container">
													<div class="input select rating-c">
														<select class="rp-stars" name="rating">
															<option value=""></option>
															<option value="1">1</option>
															<option value="2">2</option>
															<option value="3" selected="selected">3</option>
															<option value="4">4</option>
															<option value="5">5</option>
														</select>
													</div>
												</div>
											</div>
										</li>
										<li class="comments-add__item">
											<textarea cols="2" rows="2" class="textarea-text white" placeholder="Review"></textarea>
										</li>
										<li class="comments-add__item">
											<button type="Submit" class="btn shaded red medium">Submit</button>
										</li>
									</ul>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>-->
			<div class="our-blog__controls">
				<div class="container">
					<div class="row">
                   	<div class="col-lg-6">
							<?php  previous_post('%', '<span class="dark">Previeus post</span>
								', TRUE); ?>
						</div>
						<div class="col-lg-6">
							<?php  next_post('%', '<span class="dark">Next post</span>
								', TRUE); ?>
						</div>
					</div>
				</div>
			</div>
		</div><!-- end .our-blog -->
		<!--<nav class="main-nav">
			<div class="container">
				<div class="row">
					<div class="col-xs-3 wow fadeInUp animated" data-wow-delay="0.2s" data-wow-offset="50" data-wow-duration="1s">
						<p>Information</p>
						<ul class="main-nav__list">
							<li><a href="#">About Us</a></li>
							<li><a href="#">Delivery Information</a></li>
							<li><a href="#">Privacy Policy</a></li>
							<li><a href="#">Terms &amp; Conditions</a></li>
						</ul>
					</div>
					<div class="col-xs-3 wow fadeInUp animated" data-wow-delay="0.4s" data-wow-offset="50" data-wow-duration="1s">
						<p>Extras</p>
						<ul class="main-nav__list">
							<li><a href="#">Brands</a></li>
							<li><a href="#">Gift Vouchers</a></li>
							<li><a href="#">Affiliates</a></li>
							<li><a href="#">Specials</a></li>
						</ul>
					</div>
					<div class="col-xs-3 wow fadeInUp animated" data-wow-delay="0.6s" data-wow-offset="50" data-wow-duration="1s">
						<p>My account</p>
						<ul class="main-nav__list">
							<li><a href="#">My Account</a></li>
							<li><a href="#">Order History</a></li>
							<li><a href="#">Wish List</a></li>
							<li><a href="#">Newsletter</a></li>
						</ul>
					</div>
					<div class="col-xs-3 wow fadeInUp animated" data-wow-delay="0.8s" data-wow-offset="50" data-wow-duration="1s">
						<p>Customer service</p>
						<ul class="main-nav__list">
							<li><a href="#">Contact Us</a></li>
							<li><a href="#">Returns</a></li>
							<li><a href="#">Site Map</a></li>
						</ul>
					</div>
				</div>
			</div>
		</nav>--><!-- end .main-nav -->
	</div><!-- end .main -->
	
  </body>
</html>	

<?php get_footer(); ?>